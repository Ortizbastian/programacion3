﻿using CapaDatos.Tablas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace CapaDatos.Metodos
{
    public class EF
    {
      public static List<Producto> ObtenerProducto()
        {
            try
            {
                using (var paginaWeb = new PaginaWeb())
                {
                    return paginaWeb.Producto.ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<Categorias> ObtenerCategorias()
        {
            try
            {
                using (var paginaWeb = new PaginaWeb())
                {
                    return paginaWeb.Categorias.ToList();
                }
            }catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<Subcategorias> ObtenerSubcategorias()
        {
            try
            {
                using (var paginaWeb = new PaginaWeb())
                {
                    return paginaWeb.Subcategorias.Include("CreadoPor").ToList();
                }
            }catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool AgregarConsulta (string Nombre, string Correo, string Asunto, string Mensaje)
        {
            try
            {
                using (var paginaWeb = new PaginaWeb())
                {
                    var nuevaConsulta = new Consulta
                    {
                        Nombre = Nombre,
                        Correo = Correo,
                        Asunto = Asunto,
                        Mensaje = Mensaje
                    };

                    paginaWeb.Consulta.Add(nuevaConsulta);

                    paginaWeb.SaveChanges();
                }

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
