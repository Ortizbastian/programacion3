﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace CapaDatos.Tablas
{
    [Table("Consulta")]
    public class Consulta
    {
        public Consulta()
        {

        }

        [Key]
        [Column("IDconsulta", Order = 1)]
        public int IDConsulta { get; set; }
            
        [Required, MaxLength(50)]
        [Column("Nombre", Order = 2)]
        public string Nombre { get; set; }
          
        [Required, MaxLength(50)]
        [Column("Correo", Order = 3)]
        public string Correo { get; set; }

        [Required, MaxLength(20)]
        [Column("Asunto", Order = 4)]
        public string Asunto { get; set; }
        
        [Required, MaxLength(100)]
        [Column("Mensaje", Order = 5)]
        public string Mensaje { get; set; }

    }
}
