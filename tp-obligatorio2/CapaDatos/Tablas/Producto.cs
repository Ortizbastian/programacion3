﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace CapaDatos.Tablas
{
    [Table("Producto")]
    public class Producto
    {
        public Producto()
        {

        }
        [Key]
        [Column("ID", Order = 1)]
        public int ID { get; set; }

        [Required, MaxLength(50)]
        [Column("Nombre", Order = 2)]
        public string Nombre { get; set; }

        [Required]
        [Column("Precio", Order = 3)]
        public float Precio { get; set; }

        [Required, MaxLength(50)]
        [Column("Img", Order = 4)]
        public string Img { get; set; }

        [Required]
        [Column("EsOferta", Order = 5)]
        public bool EsOferta { get; set; }

        [Required]
        [Column("EsDestacado", Order = 6)]
        public bool EsDestacado { get; set; }
    }   

}
