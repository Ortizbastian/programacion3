﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaDatos.Tablas
{
    [Table("Categorias")]
    public class Categorias
    {
        public Categorias()
        {

        }
        [Key]
        [Required]
        [Column("IDCategorias", Order = 1)]
        public int IDCategorias { get; set; }

        [Required, MaxLength(50)]
        [Column("NombreCategorias", Order = 2)]
        public string NombreCategorias { get; set; }

    }
}
