﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaDatos.Tablas
{
    [Table("Subcategorias")]
    public class Subcategorias
    {
        public Subcategorias()
        {

        }
        [Key]
        [Required]
        [Column("IDSubcategoria", Order = 1)]
        public int IDSubcategoria { get; set; }
        
        [ForeignKey("CreadoPor")]
        [Column("FK_IDCategorias", Order = 2)]
        public int IDCategoria { get; set; }

        [Required, MaxLength(50)]
        [Column("NombreSubcategorias", Order = 3)]
        public string NombreSubcategorias { get; set; }
            
        //conexion
        public virtual Categorias CreadoPor { get; set; }
        
    }

}
