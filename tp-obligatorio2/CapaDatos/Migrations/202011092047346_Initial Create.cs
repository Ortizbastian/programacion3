﻿namespace CapaDatos.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Categorias",
                c => new
                    {
                        IDCategorias = c.Int(nullable: false, identity: true),
                        NombreCategorias = c.String(nullable: false, maxLength: 50),
                    })
                .PrimaryKey(t => t.IDCategorias);
            
            CreateTable(
                "dbo.Consulta",
                c => new
                    {
                        IDconsulta = c.Int(nullable: false, identity: true),
                        Nombre = c.String(nullable: false, maxLength: 50),
                        Correo = c.String(nullable: false, maxLength: 50),
                        Asunto = c.String(nullable: false, maxLength: 20),
                        Mensaje = c.String(nullable: false, maxLength: 100),
                    })
                .PrimaryKey(t => t.IDconsulta);
            
            CreateTable(
                "dbo.Producto",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Nombre = c.String(nullable: false, maxLength: 50),
                        Precio = c.Single(nullable: false),
                        Img = c.String(nullable: false, maxLength: 50),
                        EsOferta = c.Boolean(nullable: false),
                        EsDestacado = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Subcategorias",
                c => new
                    {
                        IDSubcategoria = c.Int(nullable: false, identity: true),
                        FK_IDCategorias = c.Int(nullable: false),
                        NombreSubcategorias = c.String(nullable: false, maxLength: 50),
                    })
                .PrimaryKey(t => t.IDSubcategoria)
                .ForeignKey("dbo.Categorias", t => t.FK_IDCategorias, cascadeDelete: false)
                .Index(t => t.FK_IDCategorias);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Subcategorias", "FK_IDCategorias", "dbo.Categorias");
            DropIndex("dbo.Subcategorias", new[] { "FK_IDCategorias" });
            DropTable("dbo.Subcategorias");
            DropTable("dbo.Producto");
            DropTable("dbo.Consulta");
            DropTable("dbo.Categorias");
        }
    }
}
