﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using CapaDatos.Tablas;

namespace CapaDatos
{
    public class PaginaWeb : DbContext 
    {
        public PaginaWeb() : base("name = PaginaWeb")
        {
            var ensureDLLIsCopied =
                System.Data.Entity.SqlServer.SqlProviderServices.Instance;
        }

        public DbSet<Producto> Producto { get; set; }
        
        public DbSet<Categorias> Categorias { get; set; }

        public DbSet<Subcategorias> Subcategorias { get; set; }

        public DbSet<Consulta> Consulta { get; set; }

    }
}
