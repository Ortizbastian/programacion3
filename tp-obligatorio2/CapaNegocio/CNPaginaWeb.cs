﻿using CapaDatos.Metodos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;


namespace CapaNegocio
{
    public class CNPaginaWeb
    {
        public static List<CNproductoDestacado> ObtenerProducto()
        {
            try
            {
                var capa_datos = EF.ObtenerProducto();

                return capa_datos.Select(x => new CNproductoDestacado
                {
                    ID = x.ID,
                    Nombre = x.Nombre,
                    Img = x.Img,
                    Precio = x.Precio,
                    EsOferta = x.EsOferta,
                    EsDestacado = x.EsDestacado

                }).ToList();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<CNCategorias> ObtenerCategorias()
        {
            try
            {
                var capa_datos = EF.ObtenerCategorias();

                return capa_datos.Select(x => new CNCategorias
                {
                    IDCategorias = x.IDCategorias,
                    NombreCategorias = x.NombreCategorias

                }).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<CNSubcategorias> ObtenerSubcategorias()
        {
            try
            {
                var capa_datos = EF.ObtenerSubcategorias();

                return capa_datos.Select(x => new CNSubcategorias
                {
                    IDSubcategoria = x.IDSubcategoria,
                    IDCategoria = x.IDCategoria,
                    NombreSubcategorias = x.NombreSubcategorias

                }).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool AgregarConsulta(string Nombre, string Correo, string Asunto, string Mensaje)
        {
            try
            {
                var consulta = ValidarDatos(Nombre, Correo, Asunto, Mensaje);
                if (consulta)
                {
                    var capa_datos = EF.AgregarConsulta(Nombre, Correo, Asunto, Mensaje);

                    if (capa_datos)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool ValidarDatos(string Nombre, string Correo, string Asunto, string Mensaje)
        {
            string validacionCaracteres = "[a-zA-Z]";
            string validacionCorreo = "\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*";
            try
            {
                if (Nombre.Length < 4 || Nombre.Length > 30 || (!Regex.IsMatch(Nombre, validacionCaracteres)))
                {
                    return false;
                }
                else if (!Regex.IsMatch(Correo, validacionCorreo))
                {
                    return false;
                }
                else if (Asunto.Length > 20)
                {
                    return false;
                }
                else if (Mensaje.Length < 10 || Mensaje.Length > 100)
                {
                    return false;
                }
                
                else
                {
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

    }
}
