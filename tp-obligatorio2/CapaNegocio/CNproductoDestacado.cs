﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaNegocio
{
    public class CNproductoDestacado
    {
        public int ID { get; set; }

        public string Nombre { get; set; }
        
        public string Img { get; set; }

        public float Precio { get; set; }

        public bool EsOferta { get; set; }
        
        public bool EsDestacado { get; set; }
    }
}
