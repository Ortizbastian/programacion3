﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaNegocio
{
    public class CNSubcategorias
    {
        public int IDSubcategoria { get; set; }

        public int IDCategoria { get; set; }
        
        public string NombreSubcategorias { get; set; }

    }
}
