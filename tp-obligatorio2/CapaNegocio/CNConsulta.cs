﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaNegocio
{
    public class CNConsulta
    {
        public int IDConsulta { get; set; }

        public string Nombre { get; set; }

        public string Correo { get; set; }

        public string Asunto { get; set; }

        public string Mensaje { get; set; }

    }
}
