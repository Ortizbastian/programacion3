﻿using CapaNegocio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace tp2OB.Models
{
    public class ProductosPag
    {
        public List<CNproductoDestacado> Productos { get; set; }

        public List<CNCategorias> Categorias { get; set; }

        public List<CNSubcategorias> Subcategorias { get; set; }

    }
}