﻿using CapaNegocio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Mvc;
using System.Web.WebPages;
using tp2OB.Models;

namespace tp2OB.Controllers
{
    public class ContactoController : Controller
    {
        // GET: Contacto
        public ActionResult Contacto()
        {
            
            return View();
        }

        [HttpPost]
        public ActionResult CrearConsulta(string Nombre, string Correo, string Asunto, string Mensaje)
        {
            var consulta = CNPaginaWeb.AgregarConsulta(Nombre, Correo, Asunto, Mensaje);
            if (consulta == true)
            {
                ViewBag.mensaje = "Se ha enviado la consulta ";
            }
            else
            {
                ViewBag.mensaje = "Por favor verificar los campos e intentelo nuevamente";
            }
            
            return Json(new { mensaje = ViewBag.mensaje },JsonRequestBehavior.AllowGet);
        }
          
    }
}
