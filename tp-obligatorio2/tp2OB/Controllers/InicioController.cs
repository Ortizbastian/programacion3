﻿using CapaNegocio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Security.Cryptography.X509Certificates;
using System.Web;
using System.Web.Mvc;
using tp2OB.Models;

namespace tp2OB.Controllers
{
    public class InicioController : Controller
    {

        public ActionResult Inicio()
        {
            var producto = CNPaginaWeb.ObtenerProducto();

            var categoria = CNPaginaWeb.ObtenerCategorias();

            var subcategoria = CNPaginaWeb.ObtenerSubcategorias();

            var modelo = new ProductosPag
            {
                Productos = producto.Select(x => new CNproductoDestacado
                {
                    ID = x.ID,
                    Nombre = x.Nombre,
                    Img = x.Img,
                    Precio = x.Precio,
                    EsOferta = x.EsOferta,
                    EsDestacado = x.EsDestacado

                }).ToList(),

                Categorias = categoria.Select(x => new CNCategorias
                {
                    IDCategorias = x.IDCategorias,
                    NombreCategorias = x.NombreCategorias
                }).ToList(),

                Subcategorias = subcategoria.Select(x => new CNSubcategorias
                {
                    IDSubcategoria = x.IDSubcategoria,
                    IDCategoria = x.IDCategoria,
                    NombreSubcategorias = x.NombreSubcategorias
                }).ToList()
            };

            return View(modelo);

        }

        [HttpPost]
        public void Crear(string Nombre, string Correo, string Asunto, string Mensaje)
        {
            var respuesta = CNPaginaWeb.AgregarConsulta(Nombre, Correo, Asunto, Mensaje);
        }
    }
}